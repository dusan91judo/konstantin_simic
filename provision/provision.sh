#!/usr/bin/env bash
#variables
DBPASSWD=somepassword
#scripts
mkdir /home/ubuntu/symfonytasks
mkdir /home/ubuntu/symfonytasks/var
mkdir /home/ubuntu/symfonytasks/vendor
ln -s /home/ubuntu/symfonytasks/var /vagrant/task/var
ln -s /home/ubuntu/symfonytasks/vendor /vagrant/task/vendor
sudo add-apt-repository -y ppa:ondrej/php
sudo apt -y update
sudo apt -y full-upgrade
sudo debconf-set-selections <<< "mysql-server mysql-server/root_password password $DBPASSWD"
sudo debconf-set-selections <<< "mysql-server mysql-server/root_password_again password $DBPASSWD"
sudo apt -y install nginx mysql-server php7.1 php7.1-fpm php7.1-cli php7.1-mysql php7.1-xml php7.1-json curl git unzip
curl -Ss https://getcomposer.org/installer | php
sudo mv -f composer.phar /usr/bin/composer
sudo cp -f /vagrant/provision/default /etc/nginx/sites-available/default
cd /vagrant/task/
/usr/bin/composer update
/usr/bin/php /vagrant/task/bin/console doctrine:database:create --if-not-exists
/usr/bin/php /vagrant/task/bin/console doctrine:schema:update --force
sudo systemctl enable php7.1-fpm
sudo systemctl restart php7.1-fpm
sudo systemctl enable mysql
sudo systemctl restart mysql
sudo systemctl enable nginx
sudo systemctl restart nginx


