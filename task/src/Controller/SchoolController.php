<?php

namespace App\Controller;

use App\Entity\School;
use App\Form\SchoolType;
use App\Helper\Paginator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;

class SchoolController extends Controller
{
    const PER_PAGE = 5;
    public function index(Request $request)
    {
        $perPage = self::PER_PAGE;
        $repository = $this->getDoctrine()->getRepository(School::class);
        $paginator = Paginator::getPaginator($request, $repository, $perPage);
        return $this->render('schools/index.html.twig',
            array(
                  'schools' => $repository->findAllPaginated($paginator['offset'], $perPage),
                  'paginator' => $paginator
                )
            );
    }

    public function create (Request $request){
        $school = new School();
        $newSchoolForm = $this->createForm(SchoolType::class, $school);
        $newSchoolForm->handleRequest($request);

        if ($newSchoolForm->isSubmitted() && $newSchoolForm->isValid()) {
            $school = $newSchoolForm->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($school);
            $em->flush();
            $this->addFlash(
                'success',
                'The school has been created!'
            );
        } elseif ($request->getMethod() === 'POST') {
            $this->addFlash(
                'danger',
                'The school has not been created!'
            );
        }
        return $this->render('schools/create.html.twig',
            array('form' => $newSchoolForm->createView(),
            )
        );
    }

    public function delete ($id){
        $em = $this->getDoctrine()->getManager();
        $school = $em->getRepository(School::class)->find($id);
        if (is_null($school)){
            $this->addFlash(
                'danger',
                'The school has not been deleted!'
            );
        } else {
            try {
                $em->remove($school);
                $em->flush();
                $this->addFlash(
                    'success',
                    'The school has been deleted!'
                );
            } catch (ForeignKeyConstraintViolationException $e) {
                $this->addFlash(
                    'danger',
                    'The school has not been deleted because there are related teachers!'
                );
            }
        }
        return $this->redirectToRoute('schools_index');
    }
}
