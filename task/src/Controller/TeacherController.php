<?php

namespace App\Controller;

use App\Entity\Teacher;
use App\Form\TeacherType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\DBAL\Exception\NotNullConstraintViolationException;
use App\Helper\Paginator;

class TeacherController extends Controller
{
    const PER_PAGE = 5;
    public function index(Request $request)
    {
        $perPage = self::PER_PAGE;
        $repository = $this->getDoctrine()->getRepository(Teacher::class);
        $search = $request->query->get('search', null);
        if ($search === ''){
            $search = null;
        }
        $paginator = Paginator::getPaginator($request, $repository, $perPage, $search);
        $teachers = $repository->findAllPaginated($paginator['offset'], $perPage, $search);
        return $this->render('teachers/index.html.twig',
            array(
                'teachers' => $teachers,
                'paginator' => $paginator,
                'search' => $search
            )
        );
    }

    public function create (Request $request){
        $teacher = new Teacher();
        $newTeacherForm = $this->createForm(TeacherType::class, $teacher);
        $newTeacherForm->handleRequest($request);

        $this->save($newTeacherForm, $request);


        return $this->render('teachers/create.html.twig',
            array('form' => $newTeacherForm->createView(),
            )
        );
    }

    public function edit (Request $request, $id){
        $em = $this->getDoctrine()->getManager();
        $teacher = $em->getRepository(Teacher::class)->find($id);
        if (is_null($teacher)){
            $this->addFlash(
                'danger',
                'This teacher does not exist!'
            );
            return $this->redirectToRoute('teachers_index');
        }
        $editTeacherForm = $this->createForm(TeacherType::class, $teacher);
        $editTeacherForm->handleRequest($request);
        $this->save($editTeacherForm, $request);
        return $this->render('teachers/edit.html.twig',
            array('form' => $editTeacherForm->createView(),
            )
        );

    }

    public function delete ($id){
        $em = $this->getDoctrine()->getManager();
        $teacher = $em->getRepository(Teacher::class)->find($id);
        if (is_null($teacher)){
            $this->addFlash(
                'danger',
                'The teacher has not been deleted!'
            );
        } else {
            $em->remove($teacher);
            $em->flush();
            $this->addFlash(
                'success',
                'The teacher has been deleted!'
            );
        }
        return $this->redirectToRoute('teachers_index');
    }

    private function save($form, $request){
        if ($form->isSubmitted() && $form->isValid()) {
        try {
            $teacher = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($teacher);
            $em->flush();
            $this->addFlash(
                'success',
                'The teacher has been saved!'
            );
        } catch (NotNullConstraintViolationException $e){
            $this->addFlash(
                'danger',
                'A school must be created first!'
                );
            }
        } elseif ($request->getMethod() === 'POST') {
            $this->addFlash(
                'danger',
                'The teacher has not been saved!'
            );
        }
    }
}
