<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use App\Validator\Constraints as MyAssert;


/**
 * @ORM\Entity(repositoryClass="App\Repository\SchoolRepository")
 */
class School
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(name="School_name", type="string", length=100, nullable=false)
     * @Assert\NotBlank()
     */
    private $schoolName;

    /**
     * @ORM\Column(name="Year_founded", type="string", length=4, nullable=false)
     * @Assert\NotBlank()
     * @Assert\Range(
     *      min = 1000,
     *      minMessage = "The year should be greater than 1000",
     * )
     * @MyAssert\LessThanCurrentYear()
     */
    private $yearFounded;

    /**
     * @ORM\Column(name="City", type="string", length=100, nullable=true)
     */
    private $city;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Teacher", mappedBy="school")
     */
    private $teachers;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getSchoolName()
    {
        return $this->schoolName;
    }

    /**
     * @param mixed $schoolName
     */
    public function setSchoolName($schoolName)
    {
        $this->schoolName = $schoolName;
    }

    /**
     * @return mixed
     */
    public function getYearFounded()
    {
        return $this->yearFounded;
    }

    /**
     * @param mixed $yearFounded
     */
    public function setYearFounded($yearFounded)
    {
        $this->yearFounded = $yearFounded;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return mixed
     */
    public function getTeachers()
    {
        return $this->teachers;
    }

    /**
     * @param mixed $teachers
     */
    public function setTeachers($teachers)
    {
        $this->teachers = $teachers;
    }

    public function __toString() {
        return $this->getSchoolName();
    }


}
