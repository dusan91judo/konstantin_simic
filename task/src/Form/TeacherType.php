<?php

namespace App\Form;

use App\Entity\School;
use App\Entity\Teacher;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;


class TeacherType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', TextType::class, array('required'   => true))
            ->add('lastName', TextType::class, array('required'   => true))
            ->add('birthDate', BirthdayType::class, array(
                'required'   => true,
                )
            )
            ->add('school', EntityType::class, array(
                'class' => School::class,
                'required'   => true
                )
            )
            ->add('save', SubmitType::class, array('label' => 'Save Teacher'))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // uncomment if you want to bind to a class
            'data_class' => Teacher::class,
        ]);
    }
}
