<?php
namespace App\Helper;

class Paginator {
    public static function getPaginator($request, $repository, $perPage, $search = null){
        $numRows = $repository->findAllCount($search);
        $page =  is_numeric ($request->query->get('page', 1)) && $request->query->get('page', 1) > 0 ? $request->query->get('page', 1) : 1;
        $offset = ($page - 1) * $perPage;
        $totalPages = ceil($numRows / $perPage);
        if ($offset > $numRows) {
            $page = 1;
            $offset = 0;
        }
        return array(
            'page' => $page,
            'per_page' => $perPage,
            'offset' => $offset,
            'total_pages' => $totalPages,
            'total_rows' => $numRows
        );
    }
}
