<?php

namespace App\Repository;

use App\Entity\Teacher;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class TeacherRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Teacher::class);
    }

    public function findAllPaginated($offset, $limit, $search = null)
    {
        $result = $this->createQueryBuilder('t')
            ->orderBy('t.id', 'ASC')
            ->setFirstResult($offset)
            ->setMaxResults($limit);
        $result = $this->prepareSearchQuery($result, $search);
        $result = $result->getQuery()
               ->getResult();
        return $result;
    }

    public function findAllCount($search)
    {
        $result = $this->createQueryBuilder('t')
                        ->select('COUNT(t)');
        $result = $this->prepareSearchQuery($result, $search);
        $result = $result->getQuery()
                          ->getSingleScalarResult();
        return $result;
    }

    private function prepareSearchQuery($result, $search)
    {
        if (!is_null($search)){
            $searchArray = explode (' ', urldecode($search));
            $result = $result->where('t.firstName = :firstName')
                ->setParameter('firstName', $searchArray[0]);
            if (count($searchArray) > 1){
                $result = $result->andWhere('t.lastName = :lastName')
                    ->setParameter('lastName', $searchArray[1]);
            }
        }
        return $result;
    }
}
