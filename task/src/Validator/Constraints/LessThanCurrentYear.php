<?php
namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
* @Annotation
*/
class LessThanCurrentYear extends Constraint
{
    public $message = 'The year should be less than the current one.';

    public function validatedBy()
    {
        return get_class($this).'Validator';
    }

}