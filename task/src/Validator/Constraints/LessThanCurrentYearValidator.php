<?php
namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class LessThanCurrentYearValidator extends ConstraintValidator
{

    public function validate($value, Constraint $constraint)
    {
        if ($value > date('Y')) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ string }}', $value)
                ->addViolation();
        }
    }
}